package pl.sda.AudioService;

public class Song {

    private String artist;
    private String songTitle;
    private String albumTitle;
    private int yearOfCreation;


    public Song(String artist, String songTitle) {
        this.artist = artist;
        this.songTitle = songTitle;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public int getYearOfCreation() {
        return yearOfCreation;
    }

    public void setYearOfCreation(int yearOfCreation) {
        this.yearOfCreation = yearOfCreation;
    }

    public String getKey() {
        return this.artist + " - " + this.songTitle;
    }

    @Override
    public String toString() {
//        return "Artist: " + this.artist + "; Song title: " + this.songTitle + "; Album title: " + this.albumTitle + "; Year: " + this.yearOfCreation;
        return "Artist: " + this.artist + "; Song title: " + this.songTitle;
    }
}

