package pl.sda.AudioService;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class AudioApp {

    private ArrayList<Song> songList;
    private String audioPath;

    public AudioApp(ArrayList<Song> songList, String audioPath) {
        this.songList = songList;
        this.audioPath = audioPath;
    }

    public void showMenu(){
        System.out.println("MENU:");
        System.out.println("1. Wczytaj plik");
        System.out.println("2. Znajdź wykonawcę");
        System.out.println("3. Znajdź tytuł piosenki");
        System.out.println("4. Wyświetl tekst piosenki wg tytułu");
        System.out.println("5. Wyświetl zawartość audioteki");
        System.out.println("Aby zakończyć, wpisz q");
        System.out.println("Podaj numer...");

        runMenuScanner();
    }

    public void runMenuScanner() {
        Scanner scanner = new Scanner(System.in);
        String optionString;
        while (scanner.hasNextLine() && !scanner.nextLine().equals("q")) {
            optionString = scanner.nextLine();
            switch (optionString) {
                case "1":
                    System.out.println("1");
                    break;
                case "2":
                    System.out.println("Wpisz wykonawcę:");
                    showArtist(runUserScanner());
                    break;
                case "3":
                    System.out.println("Wpisz utwór:");
                    showSongTitle(runUserScanner());
                    break;
                case "4":
                    showLyrics();
                    break;
                case "5":
                    System.out.println("Zawartość audioteki:");
                    showContentOfSongList(songList);
                    break;
                default:
                    System.out.println("Niewłaściwy znak. Podaj liczbę od 1 do 5");
            }
        }
    }

    public String runUserScanner() {
        Scanner scanner = new Scanner(System.in);
        String userQuery = scanner.nextLine();
        return userQuery;
    }


    public void showContentOfSongList(ArrayList<Song> songList) {
        for (Song s:songList) {
            System.out.println(s.toString());
        }
    }

    public void showArtist(String artistName) {
        Boolean result = false;
        for (Song s:songList) {
            if (s.getArtist().equals(artistName)) {
                result = true;
                System.out.println(s.toString());
            }
        }
        if (!result) {
            System.out.println("nie znaleziono :(");
        }
    }

    public void showSongTitle(String songTitle) {
        Boolean result = false;
        for (Song s:songList) {
            if (s.getSongTitle().equals(songTitle)) {
                result = true;
                System.out.println(s.toString());
            }
        }
        if (!result) {
            System.out.println("nie znaleziono :(");
        }
    }

    public void readFile(Path path) {
        try {
            List<String> readContent = Files.readAllLines(path);
            for (String s:readContent) {
                System.out.println(s);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void showLyrics() {

        System.out.println("Wpisz wykonawcę:");
        String searchingArtist = runUserScanner();
        System.out.println("Wpisz utwór:");
        String searchingSong = runUserScanner();
        String searchingKey = searchingArtist + " - " + searchingSong;
        Boolean result = false;

        HashMap<String, Song> songMap = new HashMap();

        for (Song s:songList) {
            songMap.put(s.getKey(), s);
        }

        for (String key:songMap.keySet()) {
            if (key.equals(searchingKey)) {
                result = true;
                break;
            }
        }

        if (result) {
            readFile(Paths.get(audioPath+"\\"+searchingKey+".txt"));
        } else {
            System.out.println("nie znaleziono :(");
        }
    }

}
