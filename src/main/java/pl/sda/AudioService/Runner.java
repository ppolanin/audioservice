package pl.sda.AudioService;

import java.util.ArrayList;

public class Runner {


    public static void main(String[] args) {

        AudioApp ap = new AudioApp(createSongList(), "d:\\Java\\AudioService");
        ap.showMenu();

    }

    public static ArrayList<Song> createSongList() {

        ArrayList<Song> songList = new ArrayList<>();
        songList.add(new Song("Jimmy Hendrix","All Along The WatchTower"));
        songList.add(new Song("Beastie Boys","Intergalactic"));
        songList.add(new Song("Pink Floyd","Time"));
        songList.add(new Song("Red Hot Chili Peppers","Californication"));
        songList.add(new Song("Beck","Looser"));
        songList.add(new Song("Beastie Boys","Sabotage"));
        songList.add(new Song("Pink Floyd","Comfortably Numb"));
        songList.add(new Song("Tool","Schism"));
        songList.add(new Song("Jane's Addiction","Jane says"));
        songList.add(new Song("Planet Funk","Big fish"));
        songList.add(new Song("The Cinematic Orchestra","The Awakening Of A Woman"));

        return  songList;

    }










}
